package com.centralway.app.volley;

import android.content.Context;

import com.android.volley.Cache.Entry;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.centralway.app.model.FeedItem;
import com.centralway.app.model.FeedList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.simpleframework.xml.core.Persister;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class FeedDownloader {

    public static final String TAG = FeedDownloader.class.getSimpleName();

    private static final String TAG_FEED = "feed";
    private static final String TAG_NAME = "name";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_LINK = "link";

    private static final String SUFFIX_JSON = "json";


    public interface FeedResponseListener<T> {

        public void onResponse(String feedUrl, T response);

        public void onErrorResponse(String feedUrl);
    }

    private final Context mContext;
    private final DownloadFeedsListener mListener;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    LruBitmapCache mLruBitmapCache;

    private int mPendingRequests;

    public interface DownloadFeedsListener {
        public void onDownloadFinished(String feedUrl, List<FeedItem> items);

        public void onPartialDownloadFinished(String feedUrl, List<FeedItem> response);
    }

    public FeedDownloader(Context context, DownloadFeedsListener listener) {
        mContext = context.getApplicationContext();
        mListener = listener;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            getLruBitmapCache();
            mImageLoader = new ImageLoader(this.mRequestQueue, mLruBitmapCache);
        }

        return this.mImageLoader;
    }

    public LruBitmapCache getLruBitmapCache() {
        if (mLruBitmapCache == null)
            mLruBitmapCache = new LruBitmapCache();
        return this.mLruBitmapCache;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }


    public void cancelPendingRequests() {
        mPendingRequests = 0;
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }

    private FeedResponseListener mResponseListener = new FeedResponseListener<List<FeedItem>>() {
        @Override
        public void onResponse(String feedUrl, List<FeedItem> response) {
            if (mPendingRequests <= 0) {
                mListener.onDownloadFinished(feedUrl, response);
            } else {
                mListener.onPartialDownloadFinished(feedUrl, response);
            }
            mPendingRequests--;
        }

        @Override
        public void onErrorResponse(String feedUrl) {
            if (mPendingRequests <= 0) {
                mListener.onDownloadFinished(feedUrl, null);
            }
            mPendingRequests--;
        }
    };

    public void downloadFeeds(List<String> feedList) {
        if (feedList == null || feedList.size() <= 0) {
            return;
        }
        mPendingRequests = feedList.size();
        for (String feed : feedList) {
            if (feed != null) {
                downloadFeedSource(feed, mResponseListener);
            }
        }
    }

    public void downloadFeed(String feedUrl) {
        downloadFeedSource(feedUrl, mResponseListener);
    }

    private void downloadFeedSource(String feedUrl, FeedResponseListener listener) {

        Request<?> request = createRequest(feedUrl, listener);

        // Adding request to volley request queue
        addToRequestQueue(request);
    }

    private Request<?> createRequest(final String feedUrl, final FeedResponseListener
            listener) {
        if (feedUrl.endsWith(SUFFIX_JSON)) {
            // TODO: Check json Feeds
            return new JsonObjectRequest(Method.GET,
                    feedUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response != null) {
                                List<FeedItem> items = parseJsonFeed(feedUrl, response,
                                        listener);
                                listener.onResponse(feedUrl, items);
                            } else {
                                listener.onResponse(feedUrl, new ArrayList());
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    listener.onErrorResponse(feedUrl);
                }
            });
        }
        return new SimpleXmlRequest(Request.Method.GET, feedUrl,
                FeedList.class,
                new Response.Listener<FeedList>() {
                    @Override
                    public void onResponse(FeedList response) {
                        if (response != null) {
                            String feedUrl = response.channel.rssLink;
                            listener.onResponse(feedUrl, response.channel.getItems());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        listener.onErrorResponse(feedUrl);
                    }
                }
        );
    }

    private void fetchDataFromCache(String feedUrl, Entry entry,
                                    FeedResponseListener listener) {
        try {
            String data = new String(entry.data, "UTF-8");
            if (feedUrl.endsWith(SUFFIX_JSON)) {
                parseFeed(feedUrl, data, listener);
            } else {
                parseXMLFeed(feedUrl, data, listener);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void parseXMLFeed(String feedUrl, String data, FeedResponseListener listener) {
        try {
            Persister serializer = new Persister();
            FeedList list = serializer.read(FeedList.class, data);
            listener.onResponse(feedUrl, list);
        } catch (Exception e) {
            listener.onErrorResponse(feedUrl);
        }
    }

    public void parseFeed(String feedUrl, String data, FeedResponseListener listener) {
        // fetch the data from cache
        try {
            parseJsonFeed(feedUrl, new JSONObject(data), listener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Parsing json reponse and passing the data to feed view list adapter
     */
    private List<FeedItem> parseJsonFeed(String feedUrl, JSONObject response,
                                         FeedResponseListener listener) {
        List<FeedItem> feedList = new ArrayList<FeedItem>();
        try {
            JSONArray feedArray = response.getJSONArray(TAG_FEED);


            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);

                FeedItem item = new FeedItem();
                item.setTitle(feedObj.getString(TAG_NAME));
                item.setContent(feedObj.getString(TAG_DESCRIPTION));
                item.setLink(feedObj.getString(TAG_LINK));

                feedList.add(item);
            }

            listener.onResponse(feedUrl, feedList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return feedList;
    }
}