package com.centralway.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.centralway.app.R;
import com.centralway.app.data.FeedContract.FeedEntry;

/**
 * Adapter in charge of retrieving the different Feed news from the database.
 * It just uses the title and the content fields to display the information in the ListView.
 *
 * Created by creinhold on 07/12/14.
 */
public class FeedListAdapter extends CursorAdapter {


    public static class ViewHolder {
        public final TextView title;
        public final TextView content;

        public ViewHolder(View view) {
            title = (TextView) view.findViewById(R.id.feedTitle);
            content = (TextView) view.findViewById(R.id.feedContent);
        }
    }

    public FeedListAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_feed_items, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();

        String title = cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_TITLE));
        viewHolder.title.setText(title);

        String content = cursor.getString(cursor.getColumnIndex(FeedEntry.COLUMN_CONTENT));
        viewHolder.content.setText(content);
    }

    /**
     * Populates the relevant information into a Intent for easy use on intents.
     *
     * @param intent the current intent to be populated
     * @param position the cursor position from where to retrieve the information
     *
     * @return a new Bundle with the feed information
     */

    public void populateIntentFromPosition(Intent intent, int position) {

        getCursor().moveToPosition(position);

        Cursor cursor = getCursor();
        intent.putExtra(FeedEntry.COLUMN_TITLE, cursor.getString(cursor.getColumnIndex
                (FeedEntry.COLUMN_TITLE)));
        intent.putExtra(FeedEntry.COLUMN_CONTENT, cursor.getString(cursor.getColumnIndex
                (FeedEntry.COLUMN_CONTENT)));
    }
}