package com.centralway.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.centralway.app.util.PreferenceManager;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class MainActivity extends FragmentActivity {

    private SearchView mSearchView;
    private EditText mEditRssLinkView;
    private ListView mFeedListView;
    private PreferenceManager mPreferences;
    private RssAdapter mAdapter;
    private List<String> mFeedList;
    private TextView mSearchViewEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPreferences = new PreferenceManager(this);
        if (BuildConfig.DEBUG) {
            List<String> urlFeeds = new ArrayList<String>();
            urlFeeds.add("http://www.srf.ch/news/bnf/rss/1890");
            urlFeeds.add("http://www.srf.ch/allgemeines/rss-feeds-von-srf");
            urlFeeds.add("http://www.digg.com/rss/index.xml");

            mPreferences.saveUrlFeeds(urlFeeds);
        }

        setContentView(R.layout.activity_main);
        initComponents();

    }

    private void initComponents() {
        mEditRssLinkView = (EditText) findViewById(R.id.edit_feed);

        View addLinkButton = findViewById(R.id.add_feed_button);
        addLinkButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddLinkClicked();
            }
        });

        initListView();
        initSearchView();
    }


    private void initSearchView() {
        // Associate searchable configuration with the SearchView
        mSearchView = (SearchView) findViewById(R.id.search_term);

        int id = mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        mSearchViewEditText = (TextView) mSearchView.findViewById(id);
        mSearchViewEditText.setBackgroundColor(Color.WHITE);

        mSearchViewEditText.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    startFeedListActivity(null);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        mEditRssLinkView.clearFocus();
        mFeedListView.requestFocus();
        super.onResume();
    }


    private OnItemClickListener mFeedItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String feedUrl = mAdapter.getItem(position);
            startFeedListActivity(feedUrl);
        }
    };

    private void initListView() {
        mFeedList = mPreferences.getUrlFeeds();
        mAdapter = new RssAdapter(this, mFeedList);

        mFeedListView = (ListView) findViewById(R.id.list_feeds);
        mFeedListView.setAdapter(mAdapter);
        mFeedListView.setOnItemClickListener(mFeedItemClickListener);
    }

    private void startFeedListActivity(String feedUrl) {
        Intent intent = new Intent(this, FeedListActivity.class);
        if (feedUrl != null) {
            intent.putExtra(FeedListActivity.TAG_FEED_URL, feedUrl);
        } else {
            intent.putExtra(FeedListActivity.TAG_SEARCH_TERM, mSearchViewEditText.getText()
                    .toString());
        }
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onAddLinkClicked() {
        String newLink = mEditRssLinkView.getText().toString();
        if (TextUtils.isEmpty(newLink) || mFeedList.contains(newLink.trim())) {
            return;
        }

        newLink = newLink.trim();
        if (checkUrl(newLink)) {
            mFeedList.add(newLink);
            mPreferences.saveUrlFeeds(mFeedList);
            mAdapter.notifyDataSetChanged();
        } else {
            Toast.makeText(this, R.string.error_message_url_not_valid, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkUrl(String newUrl) {
        Pattern URL_PATTERN = Patterns.WEB_URL;
        if (URL_PATTERN.matcher(newUrl).matches()) {
            return true;
        }

        String urlString = newUrl + "";
        if (URLUtil.isNetworkUrl(urlString)) {
            try {
                new URL(urlString);
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    private class RssAdapter extends BaseAdapter{

        private final LayoutInflater mInflater;
        List<String> mUrlList;

        public RssAdapter(Context context, List<String> feedList){
            mUrlList = feedList;
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mUrlList.size();
        }

        @Override
        public String getItem(int position) {
            return mUrlList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null){
               convertView = mInflater.inflate(R.layout.list_item_rss_feeds, parent, false);
            }

            TextView textView = (TextView) convertView.findViewById(R.id.rss_url);
            textView.setText(getItem(position));

            return convertView;
        }
    }
}
