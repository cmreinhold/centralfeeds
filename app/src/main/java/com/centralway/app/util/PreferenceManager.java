package com.centralway.app.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by creinhold on 07/12/14.
 */
public class PreferenceManager {

    private static final String APP_PREFS = "app.prefs";

    private static final String TAG_URL_FEEDS = "app.prefs.feeds.url";


    private final Context mContext;
    private final SharedPreferences mPreferences;

    public PreferenceManager(Context context) {
        mContext = context.getApplicationContext();
        mPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
    }

    public List<String> getUrlFeeds() {
        Set<String> urlFeeds = mPreferences.getStringSet(TAG_URL_FEEDS, new HashSet<String>());
        return new ArrayList<String>(urlFeeds);
    }

    public void saveUrlFeeds(List<String> currentUrlFeeds) {
        Set<String> urlFeeds = new HashSet<String>(currentUrlFeeds);
        saveUrlFeeds(urlFeeds);
    }

    private void saveUrlFeeds(Set<String> urlFeeds) {
        mPreferences.edit().putStringSet(TAG_URL_FEEDS, urlFeeds).commit();
    }
}
