package com.centralway.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION_CODES;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;


/**
 * Created by creinhold on 07/12/14.
 */
public class BrandedTextView extends TextView {

    private String mOriginalText = "";

    public BrandedTextView(Context context) {
        this(context, null, 0, 0);
    }

    public BrandedTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public BrandedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(VERSION_CODES.LOLLIPOP)
    public BrandedTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public CharSequence getText() {
        return mOriginalText != null ? mOriginalText : super.getText();
    }

    public void setTextWithSearchTerm(String query, String text) {
        mOriginalText = text;
        if (TextUtils.isEmpty(query) || TextUtils.isEmpty(text)) {
            setText(text);
            return;
        }

        String styledText = text.replaceAll(query, "<font color='red'><b>" + query +
                "</b></font>");
        setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);

    }
}
