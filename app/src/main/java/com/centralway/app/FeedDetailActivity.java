package com.centralway.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.centralway.app.data.FeedContract.FeedEntry;
import com.centralway.app.view.BrandedTextView;


public class FeedDetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_detail);
        initComponents();
    }

    private void initComponents() {
        Intent intent = getIntent();
        if (intent != null) {
            String query = intent.getStringExtra(FeedListActivity.TAG_SEARCH_TERM);
            String title = intent.getStringExtra(FeedEntry.COLUMN_TITLE);
            String content = intent.getStringExtra(FeedEntry.COLUMN_CONTENT);
            BrandedTextView mFeedTitle = (BrandedTextView) findViewById(R.id
                    .feed_detail_title);
            mFeedTitle.setTextWithSearchTerm(query, title);
            BrandedTextView mFeedContent = (BrandedTextView) findViewById(R.id.feed_detail_content);
            mFeedContent.setTextWithSearchTerm(query, content);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_feed_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
