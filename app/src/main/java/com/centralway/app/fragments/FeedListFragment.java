package com.centralway.app.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.centralway.app.FeedDetailActivity;
import com.centralway.app.FeedListActivity;
import com.centralway.app.R;
import com.centralway.app.adapter.FeedListAdapter;
import com.centralway.app.data.FeedContentProvider;
import com.centralway.app.data.FeedContract.FeedEntry;
import com.centralway.app.model.FeedItem;
import com.centralway.app.util.PreferenceManager;
import com.centralway.app.volley.FeedDownloader;
import com.centralway.app.volley.FeedDownloader.DownloadFeedsListener;

import java.util.List;

/**
 * Created by creinhold on 07/12/14.
 */
public class FeedListFragment extends Fragment implements LoaderCallbacks<Cursor>,
        DownloadFeedsListener {

    private static final String[] FEED_COLUMNS = {
            FeedEntry.TABLE_NAME + "." + FeedEntry._ID,
            FeedEntry.TABLE_NAME + "." + FeedEntry.COLUMN_TITLE,
            FeedEntry.TABLE_NAME + "." + FeedEntry.COLUMN_CONTENT
    };

    private FeedListAdapter mFeedListAdapter;
    private FeedDownloader mDownloader;
    private ListView mListView;
    private PreferenceManager mPreferences;
    private String mQuery;
    private String mFeedUrl;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mPreferences = new PreferenceManager(getActivity());

    }

    @Override
    public void onPause() {
        mDownloader.cancelPendingRequests();
        super.onPause();
    }

    private OnItemClickListener mFeedItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Intent intent = new Intent(getActivity(), FeedDetailActivity.class);
            intent.putExtra(FeedListActivity.TAG_SEARCH_TERM, mQuery);
            mFeedListAdapter.populateIntentFromPosition(intent, position);
            startActivity(intent);
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_feed_list_items, container, false);

        mFeedListAdapter = new FeedListAdapter(getActivity(), null, 0);

        mListView = (ListView) rootView.findViewById(R.id.list_feed_items);
        mListView.setAdapter(mFeedListAdapter);
        mListView.setOnItemClickListener(mFeedItemClickListener);


        mDownloader = new FeedDownloader(getActivity(), this);
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleIntent(getActivity().getIntent());
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderType, Bundle bundle) {

        if (mFeedUrl != null) {
            FeedContentProvider.getFeedsByUrl(getActivity(), FEED_COLUMNS,
                    mFeedUrl);
        }
        return FeedContentProvider.getFeedsBySearchTerm(getActivity(), FEED_COLUMNS,
                mQuery);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mFeedUrl!=null){
            mDownloader.downloadFeed(mFeedUrl);
        }else{
            mDownloader.downloadFeeds(mPreferences.getUrlFeeds());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDownloader.cancelPendingRequests();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mFeedListAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mFeedListAdapter.swapCursor(null);
    }

    @Override
    public void onDownloadFinished(String feedUrl, List<FeedItem> items) {
        if (items != null) {
            FeedContentProvider.bulkInsertFeeds(getActivity(), feedUrl, items);
        }
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void onPartialDownloadFinished(String feedUrl, List<FeedItem> items) {
        if (items != null) {
            FeedContentProvider.bulkInsertFeeds(getActivity(), feedUrl, items);
        }
    }

    public void handleIntent(Intent intent) {
        mFeedUrl = intent.getStringExtra(FeedListActivity.TAG_FEED_URL);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            mQuery = intent.getStringExtra(SearchManager.QUERY);
        } else {
            mQuery = intent.getStringExtra(FeedListActivity.TAG_SEARCH_TERM);
        }
    }
}
