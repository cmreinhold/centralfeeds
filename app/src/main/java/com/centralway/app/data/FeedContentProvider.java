package com.centralway.app.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;

import com.centralway.app.data.FeedContract.FeedEntry;
import com.centralway.app.model.FeedItem;

import java.util.List;

public class FeedContentProvider extends ContentProvider {

    private final static int MATCH_FEED = 200;

    // The URI Matcher used by this content provider.
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private FeedDbHelper mOpenHelper;

    public static final String sFeedSelection =
      //  FeedEntry.TABLE_NAME
      //              + "." + FeedEntry.COLUMN_TITLE +" LIKE '?' OR " +
        FeedEntry.TABLE_NAME
                    + "." + FeedEntry.COLUMN_CONTENT + " LIKE '?'";

//    " instr(" + FeedEntry.TABLE_NAME
//                    + "." + FeedEntry.COLUMN_TITLE + ", '?') > 0 " +
//                    " OR instr( " + FeedEntry.TABLE_NAME
//                    + "." + FeedEntry.COLUMN_CONTENT + ", '?') > 0 ";


    public static final String sFeedByUrlSelection =
            " " + FeedEntry.TABLE_NAME
                    + "." + FeedEntry.COLUMN_FEED_URL + " = '?'";

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = FeedContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, FeedContract.PATH_FEED, MATCH_FEED);
        return matcher;
    }


    @Override
    public boolean onCreate() {
        mOpenHelper = new FeedDbHelper(getContext());
        return true;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        Cursor cursor;
        switch (sUriMatcher.match(uri)) {

            case MATCH_FEED: {
                cursor = mOpenHelper.getReadableDatabase().query(
                        FeedEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case MATCH_FEED:
                return FeedEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case MATCH_FEED: {
                long _id = db.insert(FeedEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = FeedEntry.buildFeedUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

            getContext().getContentResolver().notifyChange(uri, null);

        return returnUri;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        switch (match) {
            case MATCH_FEED:
                rowsDeleted = db.delete(
                        FeedEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if ( selection == null || rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case MATCH_FEED:
                rowsUpdated = db.update(FeedEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case MATCH_FEED:
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(FeedEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }


    public static Loader<Cursor> getFeedsByUrl(Context context, String[] projection,
                                               String mFeedUrl) {
        String sortOrder = FeedEntry._ID + " DESC";

        String[] selectionArgs = null;
        String selection = null;
        if (mFeedUrl != null && !mFeedUrl.trim().isEmpty()) {
            selectionArgs = new String[]{mFeedUrl};
            selection = sFeedByUrlSelection;
        }

        return new CursorLoader(
                context,
                FeedEntry.CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                sortOrder
        );
    }

    public static Loader<Cursor> getFeedsBySearchTerm(Context context, String[] projection,
                                                      String searchTerm) {
        String sortOrder = FeedEntry._ID + " DESC";

        String selection = null;
        if (!TextUtils.isEmpty(searchTerm)) {
            selection = FeedEntry.TABLE_NAME
                    + "." + FeedEntry.COLUMN_TITLE + " LIKE '%" + searchTerm + "%' OR " +
                    FeedEntry.TABLE_NAME
                    + "." + FeedEntry.COLUMN_CONTENT +" LIKE '%" + searchTerm + "%'";
         }


        return new CursorLoader(
                context,
                FeedEntry.CONTENT_URI,
                projection,
                selection,
                null,
                sortOrder
        );
    }

    public static void bulkInsertFeeds(Context context, String feedUrl, List<FeedItem> items) {
        ContentValues[] values = new ContentValues[items.size()];
        FeedItem item;
        for (int i = 0; i < items.size(); i++) {
            ContentValues contentValues = new ContentValues();
            item = items.get(i);
            contentValues.put(FeedEntry.COLUMN_TITLE, item.getTitle());
            contentValues.put(FeedEntry.COLUMN_CONTENT, item.getContent());
            contentValues.put(FeedEntry.COLUMN_WEB_URL, item.getLink());
            contentValues.put(FeedEntry.COLUMN_FEED_URL, feedUrl);
            values[i] = contentValues;
        }
        context.getContentResolver().bulkInsert(FeedEntry.CONTENT_URI, values);
        context.getContentResolver().notifyChange(FeedEntry.CONTENT_URI, null);
    }

}
