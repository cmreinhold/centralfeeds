package com.centralway.app.data;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.centralway.app.data.FeedContract.FeedEntry;

/**
 * Manages a local database for weather data.
 * <p/>
 * Created by creinhold on 06/12/14.
 */
public class FeedDbHelper extends SQLiteOpenHelper {

    // For upgrades on the database, simply upgrade
    private static final int DATABASE_VERSION = 1;

    private static final boolean DATABASE_ONLINE_DATA_ONLY = true;

    public static final String DATABASE_NAME = "weather.db";

    public FeedDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public FeedDbHelper(Context context, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, errorHandler);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        createFeedContractFeedTable(db);
    }

    private void createFeedContractFeedTable(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_FEED_TABLE = "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +

                FeedEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                FeedEntry.COLUMN_TITLE + " TEXT NOT NULL, " +
                FeedEntry.COLUMN_CONTENT + " TEXT NOT NULL, " +
                FeedEntry.COLUMN_FEED_URL + " TEXT NOT NULL, " +
                FeedEntry.COLUMN_WEB_URL + " TEXT, " +

                " UNIQUE (" + FeedEntry.COLUMN_TITLE + ", " +
                FeedEntry.COLUMN_CONTENT + ") ON CONFLICT REPLACE"
                + " );";

        sqLiteDatabase.execSQL(SQL_CREATE_FEED_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (DATABASE_ONLINE_DATA_ONLY) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME);
        }
        onCreate(sqLiteDatabase);
    }
}
