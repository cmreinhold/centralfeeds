package com.centralway.app.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Defines table and column names for the feed database.
 * <p/>
 * Created by creinhold on 06/12/14.
 */
public class FeedContract {

    public static final String CONTENT_AUTHORITY = "com.centralway.app.provider.feedcontentprovider";


    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_FEED = "feed";

    /* Inner class that defines the table contents of the location table */
    public static final class FeedEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_FEED).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_FEED;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_FEED;

        // Table name
        public static final String TABLE_NAME = "feed";

        // The title of the feed.
        public static final String COLUMN_TITLE = "title";

        // Content of the feed. Can be html or not.
        public static final String COLUMN_CONTENT = "content";

        // Link to the browseable article, if any
        public static final String COLUMN_WEB_URL = "web_url";

        // Link to the original source of information, the server feed
        public static final String COLUMN_FEED_URL = "feed_url";


        public static Uri buildFeedUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }


}
