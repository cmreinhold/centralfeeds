package com.centralway.app.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by creinhold on 07/12/14.
 */
@Root(name = "rss") //root of the xml file
@Default(value = DefaultType.FIELD)
public class FeedList {


    @Attribute(name = "version")
    private String rssVersion;

    @Element(name = "channel")
    public Channel channel;

    @Default(value = DefaultType.FIELD)
    public static class Channel {
        @Element(name = "title", required = false)
        private String rssTitle;

        @Element(name = "link", required = false)
        public String rssLink;

        @Element(name = "description", required = false)
        private String rssDescription;

        @Element(name = "language", required = false)
        private String rssLanguage;

        @Element(name = "lastBuildDate", required = false)
        private String rssLastBuildDate;

        @Element(name = "pubDate", required = false)
        private String rssPubDate;


        @Element(name = "ttl", required = false)
        private String rssTtl;

        @ElementList(entry = "item", inline = true, required = false)
        private List<FeedItem> items;

        public List<FeedItem> getItems() {
            return items;
        }
    }
}
