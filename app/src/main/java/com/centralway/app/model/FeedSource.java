package com.centralway.app.model;

/**
 * Created by creinhold on 06/12/14.
 */
public class FeedSource {

    public String mTitle;
    public String mType = "";
    public String mFeedUrl;
    public String mWebUrl;
    public boolean mEnabled = true;


    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }


    public void setType(String type) {
        mType = type;
    }

    public String getType() {
        return mType;
    }

    public void setFeedUrl(String feedUrl) {
        mFeedUrl = feedUrl;
    }

    public String getFeedUrl() {
        return mFeedUrl;
    }

    public void setWebUrl(String webUrl) {
        mWebUrl = webUrl;
    }

    public String getWebUrl() {
        return mWebUrl;
    }

    public void setEnabled(boolean isEnabled) {
        mEnabled = isEnabled;
    }

    public boolean isEnabled() {
        return mEnabled;
    }
}
