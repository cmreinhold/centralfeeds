package com.centralway.app.model;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Element;

/**
 * Created by creinhold on 06/12/14.
 */
@Default(value = DefaultType.FIELD)
public class FeedItem {

    @Element(name = "title", type = String.class, required = false)
    private String mTitle;

    @Element(name = "description", type = String.class, required = false)
    private String mContent;

    @Element(name = "pubDate", type = String.class, required = false)
    private String mTimeStamp;

    @Element(name = "guid", required = false)
    private String mGuid;

    @Element(name = "category", required = false)
    private String mCategory;

    @Element(name = "link", type = String.class, required = false)
    private String mLink;

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setContent(String htmlContent) {
        mContent = htmlContent != null ? htmlContent : "";
    }

    public String getContent() {
        return mContent;
    }

    public void setTimeStamp(String timeStamp) {
        mTimeStamp = timeStamp;
    }

    public String getTimeStamp() {
        return mTimeStamp;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getLink() {
        return mLink;
    }
}
