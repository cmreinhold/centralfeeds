/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.centralway.app;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.AndroidTestCase;

import com.centralway.app.data.FeedContract.FeedEntry;


public class TestProvider extends AndroidTestCase {

    // brings our database to an empty state
    public void deleteAllRecords() {
        mContext.getContentResolver().delete(
                FeedEntry.CONTENT_URI,
                null,
                null
        );

        Cursor cursor = mContext.getContentResolver().query(
                FeedEntry.CONTENT_URI, null, null, null, null);
        assertEquals(0, cursor.getCount());
        cursor.close();
    }

    // Since we want each test to start with a clean slate, run deleteAllRecords
    // in setUp (called by the test runner before each test).
    public void setUp() {
        deleteAllRecords();
    }

    public void testInsertReadProvider() {
        ContentValues feedTestValues = TestDb.createFeedTestValues();

        Uri weatherInsertUri = mContext.getContentResolver()
                .insert(FeedEntry.CONTENT_URI, feedTestValues);
        assertTrue(weatherInsertUri != null);

        // A cursor is your primary interface to the query results.
        Cursor weatherCursor = mContext.getContentResolver().query(
                FeedEntry.CONTENT_URI, null, null, null, null);

        TestDb.validateCursor(weatherCursor, feedTestValues);
    }

    public void testGetType() {
        // content://com.centralway.app.provider.feedcontentprovider/feed/
        String type = mContext.getContentResolver().getType(FeedEntry.CONTENT_URI);
        // vnd.android.cursor.dir/com.centralway.app.provider.feedcontentprovider/feed
        assertEquals(FeedEntry.CONTENT_TYPE, type);
    }
}
