/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.centralway.app;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import com.centralway.app.data.FeedContract.FeedEntry;
import com.centralway.app.data.FeedDbHelper;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TestDb extends AndroidTestCase {

    public static final String LOG_TAG = TestDb.class.getSimpleName();

    public void testCreateDb() throws Throwable {
        mContext.deleteDatabase(FeedDbHelper.DATABASE_NAME);
        SQLiteDatabase db = new FeedDbHelper(mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());
        db.close();
    }

    public void testInsertReadDb() {
        FeedDbHelper dbHelper = new FeedDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        testInsertReadFeedDb(db);

        dbHelper.close();
    }

    public static ContentValues createFeedTestValues() {

        // Dummy feed data
        String testFeedTitle = "Good news are here";
        String testFeedContent = "There was an accident in the main road in Memphis.";

        ContentValues values = new ContentValues();
        values.put(FeedEntry.COLUMN_TITLE, testFeedTitle);
        values.put(FeedEntry.COLUMN_CONTENT, testFeedContent);

        return values;
    }


    public static void validateCursor(Cursor valueCursor, ContentValues expectedValues) {

        assertTrue(valueCursor.moveToFirst());

        Set<Entry<String, Object>> valueSet = expectedValues.valueSet();
        for (Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            int idx = valueCursor.getColumnIndex(columnName);
            assertFalse(idx == -1);
            String expectedValue = entry.getValue().toString();
            assertEquals(expectedValue, valueCursor.getString(idx));
        }
        valueCursor.close();
    }


    private void testInsertReadFeedDb(SQLiteDatabase db) {
        ContentValues testFeedContentValues = createFeedTestValues();

        long feedRowId  = db.insert(FeedEntry.TABLE_NAME, null, testFeedContentValues);

        // Verify we got a row back.
        assertTrue(feedRowId != -1);

        Log.d(LOG_TAG, "New row id: " + feedRowId);

        Cursor cursor = db.query(FeedEntry.TABLE_NAME,
                null, null, null, null, null, null);

        validateCursor(cursor, testFeedContentValues);

        cursor.close();
    }
}
